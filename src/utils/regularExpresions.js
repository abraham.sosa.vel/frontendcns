export const regexOnlyLetters =
  /^([A-Za-zÑñÁáÉéÍíÓóÚú]*[A-Za-zÑñÁáÉéÍíÓóÚú]+)(\s+([A-Za-zÑñÁáÉéÍíÓóÚú]*[A-Za-zÑñÁáÉéÍíÓóÚú]*))*$/;
export const regexEmail =
  /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;
export const regexPassword = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;

export const regexLetterNumber = /^[a-zA-Z0-9À-ÿ\s]+$/;

export const regexNumber = /^\d*$/;
