import axios from "axios";
const API_BASE_URL = `https://3e40-2800-cd0-2401-b200-8d8c-f94e-d7c-6d67.ngrok-free.app`;
export const get = async (url, params) => {
  try {
    const response = await axios.get(`${API_BASE_URL}${url}`, { params });
    return response.data;
  } catch (error) {
    throw {
      message: error?.response?.data?.message,
      status: error?.response?.status,
      errorText: error?.response?.statusText,
    };
  }
};

export const post = async (url, data) => {
  try {
    const response = await axios.post(`${API_BASE_URL}${url}`, data);
    return response.data;
  } catch (error) {
    throw {
      message: error?.response?.data?.message,
      status: error?.response?.status,
      errorText: error?.response?.statusText,
    };
  }
};

export const put = async (url, data) => {
  try {
    const response = await axios.put(`${API_BASE_URL}${url}`, data);
    return response.data;
  } catch (error) {
    throw {
      message: error?.response?.data?.message,
      status: error?.response?.status,
      errorText: error?.response?.statusText,
    };
  }
};

export const del = async (url) => {
  try {
    const response = await axios.delete(`${API_BASE_URL}${url}`);
    return response.data;
  } catch (error) {
    throw {
      message: error?.response?.data?.message,
      status: error?.response?.status,
      errorText: error?.response?.statusText,
    };
  }
};
