import React from "react";

import { Grid, Paper } from "@mui/material";
import TitlePage from "../../components/TitlePage";
import { Chart } from "react-google-charts";
import { useSelector } from "react-redux";
const General = () => {
  const { data } = useSelector(({ generalReporter }) => generalReporter);
  return (
    <Grid container justifyContent="space-between" spacing={2}>
      {Object.keys(data).map((ciudad) => {
        console.log(data[ciudad]);
        return (
          <Grid item xs={12} lg={6}>
            <Paper sx={{ p: 2, pt: 3 }} elevation={0}>
              <Grid sx={{ mb: 1 }}>
                <TitlePage title={ciudad} />
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Chart
                    chartType="Bar"
                    data={[["Empresa", "Cantidad"], ...data[ciudad]]}
                    width="100%"
                    height="400px"
                    legendToggle
                  />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default General;
