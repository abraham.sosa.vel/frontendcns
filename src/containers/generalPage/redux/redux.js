import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  data: [],
};
export const generalSlice = createSlice({
  name: "General",
  initialState,
  reducers: {
    initialRequestGeneralSuccess: (state, action) => {
      state.data = action.payload;
    },
  },
});

export const { initialRequestGeneralSuccess } = generalSlice.actions;
export default generalSlice.reducer;
