import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import { get } from "../../../utils/request";
import { initialRequestGeneralSuccess } from "./redux";

export const initialRequestGeneralStart = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(showGlobalLoader());
      const response = await get("/MonthlyForm/getStatics");
      const nuevoObj = {};

      // Iterar a través del objeto original
      for (const region in response) {
        if (response.hasOwnProperty(region)) {
          const empresas = response[region];

          // Iterar a través de las empresas en cada región
          for (const empresa of empresas) {
            const nombreEmpresa = Object.keys(empresa)[0]; // Obtener el nombre de la empresa
            const idEmpresa = empresa[nombreEmpresa]; // Obtener el id de la empresa

            // Verificar si la región ya existe en el nuevo objeto
            if (!nuevoObj.hasOwnProperty(region)) {
              nuevoObj[region] = [];
            }

            // Agregar la empresa y su id en el nuevo objeto
            nuevoObj[region].push([nombreEmpresa, idEmpresa]);
          }
        }
      }

      dispatch(initialRequestGeneralSuccess(nuevoObj));
      dispatch(messageSuccess(`Se trajo los datos exitosamente`));
    } catch (error) {
      dispatch(messageError(`No se pudo traer los datos`));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
