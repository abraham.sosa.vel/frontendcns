import {
  hideGlobalLoader,
  messageError,
  messageSuccess,
  showGlobalLoader,
} from "../../../redux/globalRedux";
import { post } from "../../../utils/request";
import { signInSuccess } from "./redux";

export const signInStart = ({ email, password }) => {
  return async (dispatch) => {
    try {
      dispatch(showGlobalLoader());
      const request = await post("/auth/signIn", { email, password });
      localStorage.setItem("userData", JSON.stringify(request));
      dispatch(signInSuccess(request));
      dispatch(messageSuccess("Inicio de sesión exitoso"));
    } catch (error) {
      dispatch(messageError(error.message));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
