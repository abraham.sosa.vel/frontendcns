import { useState } from "react";
import { regexEmail, regexPassword } from "../../../utils/regularExpresions";

const useValidations = () => {
  const [message, setMessages] = useState({
    email: "",
    password: "",
  });
  const validationEmail = (value) => {
    if (value === "") {
      setMessages({ ...message, email: "El campo es requerido" });
      return false;
    }
    if (!regexEmail.test(value)) {
      setMessages({ ...message, email: "El email ingresado debe ser valido" });
      return false;
    }
    return true;
  };

  const validatePassword = (value) => {
    if (value === "") {
      setMessages({ ...message, password: "El campo es requerido" });
      return false;
    }
    if (value.length < 8 || value.length > 15) {
      setMessages({
        ...message,
        password: "La contraseña debe ser menor a 15 y mayor a 8 caracteres",
      });
      return false;
    }
    if (!regexPassword.test(value)) {
      setMessages({
        ...message,
        password:
          "La contraseña debe contener una letra mayúscula,minúscula y un numero",
      });
      return false;
    }
    return true;
  };
  return {
    message,
    validationEmail,
    validatePassword,
  };
};

export default useValidations;
