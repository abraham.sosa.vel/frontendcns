import { Paper, Typography, useTheme } from "@mui/material";
import React from "react";
import MonthlyForm from "./components/MonthlyForm";
import { useDispatch } from "react-redux";
import { sendMonthlyFormStart } from "./redux/thunk";
import { useNavigate } from "react-router-dom";

const MonthlySeelement = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const theme = useTheme();
  const submitForm = (e) => {
    console.log(e);
    e.companyId = e?.companyId?.id;
    e.regionalId = e?.regionalId?.id;

    dispatch(sendMonthlyFormStart(e, navigate));
  };
  return (
    <>
      <Paper sx={{ pt: 3, px: 3 }}>
        <Typography
          variant="h5"
          align="center"
          sx={{ fontWeight: "bold", color: theme.palette.text.textSepdavi }}
        >
          Caja Petrolera de Salud
        </Typography>
        <Typography variant="h6" align="center">
          LIQUIDACIÓN MENSUAL DE APORTES
        </Typography>
      </Paper>
      <Paper>
        <MonthlyForm onSubmit={submitForm} />
      </Paper>
    </>
  );
};

export default MonthlySeelement;
