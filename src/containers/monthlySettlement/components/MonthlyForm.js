import { Autocomplete, Button, Grid, Skeleton, TextField } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import StepperForm from "./StepperForm";

const MonthlyForm = ({ onSubmit }) => {
  const [activeButton, setActiveButton] = useState(false);
  const { register, control, handleSubmit } = useForm({
    mode: "onChange",
  });
  const loaderOccupation = false;
  const occupations = [
    { id: 1, label: "Cochabamba" },
    { id: 2, label: "La Paz" },
    { id: 3, label: "Oruro" },
    { id: 4, label: "Santa Cruz" },
    { id: 5, label: "Tarija" },
    { id: 6, label: "Sucre" },
    { id: 7, label: "Camiri" },
    { id: 8, label: "Yacuiba" },
    { id: 9, label: "Villamontes" },
    { id: 10, label: "Bermejo" },
    { id: 11, label: "Riberalta" },
    { id: 12, label: "Guayaramerin" },
    { id: 13, label: "Cobija" },
    { id: 14, label: "Potosi" },
    { id: 15, label: "Trinidad" },
  ];

  const categories = [
    { id: 1, label: "Instituciones Públicas SIGEP" },
    { id: 2, label: "Instituciones Públicas" },
    { id: 3, label: "Instituciones o Empresas Privadas" },
    { id: 4, label: "Caja Petrolera de Salud" },
    { id: 5, label: "Rentistas Públicos (SENASIR)" },
    {
      id: 6,
      label:
        "Rentistas Privados (AFP'S FUTURO Y PREVISION, LA VITALICIA Y OTROS)",
    },
    { id: 6, label: "Asegurados voluntarios" },
  ];

  return (
    <Grid container direction="column" sx={{ px: 4, pb: 4 }}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={2} justifyContent={"center"}>
          <Grid item xs={12} md={5} lg={4}>
            <Controller
              control={control}
              name="regionalId"
              render={({ field: { onChange, value } }) =>
                loaderOccupation ? (
                  <Skeleton variant="rounded" height={55} />
                ) : (
                  <Autocomplete
                    placeholder="Departamental"
                    value={value}
                    onChange={(event, newValue) => {
                      onChange(newValue);
                    }}
                    options={occupations}
                    isOptionEqualToValue={(option, value) =>
                      option.id === value.id
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Departamental"
                        fullWidth
                        margin="normal"
                      />
                    )}
                  />
                )
              }
            />
          </Grid>
          <Grid item xs={12} md={5} lg={4} sx={{ mt: 2 }}>
            <Controller
              control={control}
              name="date"
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  disableFuture
                  slotProps={{ textField: { fullWidth: true } }}
                  label="fecha"
                  views={["year", "month", "day"]}
                  value={value}
                  onChange={onChange}
                  textField={(params) => <TextField {...params} />}
                />
              )}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justifyContent={"center"}>
          <Grid item xs={12} md={10} lg={4}>
            <Controller
              control={control}
              name="category"
              render={({ field: { onChange, value } }) => (
                <Autocomplete
                  placeholder="Categoría de empresa"
                  value={value}
                  onChange={(event, newValue) => {
                    onChange(newValue);
                  }}
                  options={categories}
                  isOptionEqualToValue={(option, value) =>
                    option.id === value.id
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Categoría de empresa"
                      fullWidth
                      margin="normal"
                    />
                  )}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={10} lg={4}>
            <TextField
              label="Nombre o razón social"
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("companyId")}
              type="text"
              autoComplete="companyId"
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justifyContent={"center"}>
          <Grid item xs={12} md={10} lg={8}>
            <TextField
              label="Domicilio legal de la empresa"
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("direction")}
              type="text"
              autoComplete="direction"
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justifyContent={"center"}>
          <Grid item xs={12} md={4} lg={3} sx={{ mt: 1 }}>
            <TextField
              label="Teléfono"
              variant="outlined"
              margin="dense"
              fullWidth
              {...register("phone")}
              type="number"
              autoComplete="phone"
            />
          </Grid>
          <Grid item xs={12} md={6} lg={5}>
            <TextField
              label="Nro. patronal"
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("patronalNumber")}
              type="text"
              autoComplete="patronalNumber"
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} justifyContent={"center"}>
          <Grid item xs={12} md={5} lg={4}>
            <TextField
              label="Matricula"
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("enrollment")}
              type="text"
              autoComplete="enrollment"
            />
          </Grid>
          <Grid item xs={12} md={5} lg={4}>
            <TextField
              label="NIT"
              variant="outlined"
              margin="normal"
              fullWidth
              {...register("nit")}
              type="text"
              autoComplete="nit"
            />
          </Grid>
        </Grid>
        {activeButton && (
          <Grid container sx={{ mt: 2 }} justifyContent="center" spacing={2}>
            <Grid xs={12} md={5} lg={3}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
              >
                Enviar
              </Button>
            </Grid>
          </Grid>
        )}
      </form>
      <Grid container justifyContent="center" sx={{ mt: 4 }}>
        <StepperForm setActiveButton={setActiveButton} />
      </Grid>
    </Grid>
  );
};

export default MonthlyForm;
