import { Grid, Step, StepLabel, Stepper } from "@mui/material";
import React, { useState } from "react";
import EmployerDeclaration from "./EmployerDeclaration";
import SettlementQuotes from "./SettlementQuotes";
import AmountReceived from "./AmountReceived";
const steps = [
  "DECLARACIÓN DE EMPLEADOR O ASEGURADO VOLUNTARIO",
  "LIQUIDACIÓN DE COTIZACIONES C.P.S.",
  "MONTO RECIBIDO EN CAJA",
];
const StepperForm = ({ setActiveButton }) => {
  const [activeStep, setActiveStep] = useState(0);
  const renderStepContent = (step, handleNext, handleBack, setActiveButton) => {
    switch (step) {
      case 0:
        return <EmployerDeclaration handleNext={handleNext} />;
      case 1:
        return (
          <SettlementQuotes handleNext={handleNext} handleBack={handleBack} />
        );
      case 2:
        return (
          <AmountReceived
            handleNext={handleNext}
            handleBack={handleBack}
            setActiveButton={setActiveButton}
          />
        );
      default:
        return <div>Not Found</div>;
    }
  };
  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };
  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };
  return (
    <Grid item xs={12} md={10} lg={8}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <Grid container justifyContent="center" sx={{ mt: 4 }}>
        <Grid item xs={12} md={12}>
          {renderStepContent(
            activeStep,
            handleNext,
            handleBack,
            setActiveButton
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default StepperForm;
