import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  Box,
  TextField,
  Grid,
  Button,
} from "@mui/material";
import { Controller, useForm } from "react-hook-form";
import { DatePicker } from "@mui/x-date-pickers";
import { useDispatch } from "react-redux";
import { EmployerDeclarationSuccess } from "../redux/redux";

const EmployerDeclaration = ({ handleNext }) => {
  const dispatch = useDispatch();
  const { register, control, handleSubmit } = useForm({
    mode: "onChange",
  });
  const onSubmit = (data) => {
    dispatch(EmployerDeclarationSuccess(data));
    handleNext();
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box sx={{ overflow: "auto" }}>
        <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
          <TableContainer
            sx={{
              minWidth: "50rem",
              maxHeight: "65vh",
              "&::-webkit-scrollbar": {
                width: "0.35em",
                height: "0.35em",
              },
              "&::-webkit-scrollbar-track": {
                "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: 5,
              },
            }}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={5}
                  >
                    <Typography
                      align="center"
                      variant="h6"
                      sx={{ fontWeight: 500 }}
                      padding="normal"
                    >
                      DECLARACIÓN DE EMPLEADOR O ASEGURADO VOLUNTARIO
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Nro. de trabajadores
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Mes y año de la plantilla
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Salarios cotizables del mes en Bs.
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Tasa %
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Cotización Bs.
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Nro. de trabajadores"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("numberWorkers")}
                      type="number"
                      autoComplete="name"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <Controller
                      control={control}
                      name="dateTemplate"
                      render={({ field: { onChange, value } }) => (
                        <DatePicker
                          disableFuture
                          slotProps={{ textField: { fullWidth: true } }}
                          label="Mes y año "
                          views={["year", "month", "day"]}
                          value={value}
                          onChange={onChange}
                          textField={(params) => <TextField {...params} />}
                        />
                      )}
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Salarios cotizables del mes en Bs."
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("salary")}
                      type="number"
                      autoComplete="salary"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa %"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("tasa")}
                      type="number"
                      autoComplete="tasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Cotización Bs."
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("price")}
                      type="number"
                      autoComplete="price"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={4}
                  >
                    <Typography align="center" sx={{ fontWeight: 600 }}>
                      Deducciones
                    </Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography align="center">Enfermedad 75%</Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Enfermedad"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("disease")}
                      type="number"
                      autoComplete="disease"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography align="center">Maternidad 90%</Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Maternidad"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("maternity")}
                      type="number"
                      autoComplete="maternity"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography align="center">R. Profesionales 90%</Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="R. Profesionales"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("professionals")}
                      type="number"
                      autoComplete="professionals"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography align="center">Otros</Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Otros"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("other")}
                      type="number"
                      autoComplete="other"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography align="center">
                      Menos total deducciones
                    </Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Menos total deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("totalDiscount")}
                      type="number"
                      autoComplete="totalDiscount"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  ></TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={3}
                  >
                    <Typography variant="h5" sx={{ fontWeight: 500 }}>
                      TOTAL:
                    </Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("total")}
                      type="number"
                      autoComplete="total"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={5}
                  >
                    <Typography
                      align="center"
                      variant="h6"
                      sx={{ fontWeight: 500 }}
                    >
                      Declaro que todos los datos consignados, representan el
                      total de plantillas pagadas en el mes
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
      <Grid container sx={{ mt: 2 }} justifyContent="center" spacing={2}>
        <Grid item xs={12} md={5} lg={3}>
          <Button type="submit" fullWidth variant="contained" color="primary">
            Siguiente
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default EmployerDeclaration;
