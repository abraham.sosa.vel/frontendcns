import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  Box,
  TextField,
  Grid,
  Button,
  InputAdornment,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { AmountReceivedSuccess } from "../redux/redux";
const AmountReceived = ({ handleBack, setActiveButton }) => {
  const dispatch = useDispatch();
  const { register, control, handleSubmit } = useForm({
    mode: "onChange",
  });
  const onSubmit = (data) => {
    dispatch(AmountReceivedSuccess(data));
    setActiveButton(true);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box sx={{ overflow: "auto" }}>
        <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
          <TableContainer
            sx={{
              minWidth: "50rem",
              maxHeight: "65vh",
              "&::-webkit-scrollbar": {
                width: "0.35em",
                height: "0.35em",
              },
              "&::-webkit-scrollbar-track": {
                "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: 5,
              },
            }}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={5}
                  >
                    <Typography
                      align="center"
                      variant="h6"
                      sx={{ fontWeight: 500 }}
                      padding="normal"
                    >
                      MONTO RECIBIDO EN CAJA
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    colSpan={4}
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Efectivo
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      variant="outlined"
                      margin="normal"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">Bs.</InputAdornment>
                        ),
                      }}
                      fullWidth
                      {...register("cash")}
                      type="number"
                      autoComplete="cash"
                    />
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Banco
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Nombre del Banco"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("bank")}
                      type="text"
                      autoComplete="bank"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Cheque No.
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Numero"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("checkNumber")}
                      type="text"
                      autoComplete="checkNumber"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      variant="outlined"
                      margin="normal"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">Bs.</InputAdornment>
                        ),
                      }}
                      fullWidth
                      {...register("cashBank1")}
                      type="number"
                      autoComplete="cashBank1"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Banco
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Nombre del banco"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("bank2")}
                      type="text"
                      autoComplete="bank2"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Cheque No.
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Numero"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("checkNumber2")}
                      type="text"
                      autoComplete="checkNumber2"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      variant="outlined"
                      margin="normal"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">Bs.</InputAdornment>
                        ),
                      }}
                      fullWidth
                      {...register("cashBank2")}
                      type="number"
                      autoComplete="cashBank2"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    colSpan={4}
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Total Recibido en Caja
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">Bs.</InputAdornment>
                        ),
                      }}
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("totalCash")}
                      type="number"
                      autoComplete="totalCash"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    colSpan={4}
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <Typography align="center" sx={{ fontWeight: 600 }}>
                      IMPORTANTE:
                    </Typography>
                    <Typography align="center">
                      ESTE DOCUMENTO SOLO TIENE VALOR SI LLEVA LA FIRMA DEL
                      CAJERO Y SELLO DE LA CAJA
                    </Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <Typography align="center">Son:</Typography>
                    <TextField
                      label="Total"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            /Bolivianos
                          </InputAdornment>
                        ),
                      }}
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("amountTotal")}
                      type="text"
                      autoComplete="amountTotal"
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
      <Grid container sx={{ mt: 2 }} justifyContent="center" spacing={2}>
        <Grid item xs={12} md={5} lg={3}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            onClick={() => handleBack()}
          >
            Anterior
          </Button>
        </Grid>
        <Grid item xs={12} md={5} lg={3}>
          <Button type="submit" fullWidth variant="contained" color="primary">
            Siguiente
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default AmountReceived;
