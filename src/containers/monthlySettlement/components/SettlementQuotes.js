import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  Box,
  TextField,
  Grid,
  Button,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { SettlementQuotesSuccess } from "../redux/redux";
const SettlementQuotes = ({ handleNext, handleBack }) => {
  const dispatch = useDispatch();

  const { register, control, handleSubmit } = useForm({
    mode: "onChange",
  });
  const onSubmit = (data) => {
    dispatch(SettlementQuotesSuccess(data));
    handleNext();
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box sx={{ overflow: "auto" }}>
        <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
          <TableContainer
            sx={{
              minWidth: "50rem",
              maxHeight: "65vh",
              "&::-webkit-scrollbar": {
                width: "0.35em",
                height: "0.35em",
              },
              "&::-webkit-scrollbar-track": {
                "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: 5,
              },
            }}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                    colSpan={5}
                  >
                    <Typography
                      align="center"
                      variant="h6"
                      sx={{ fontWeight: 500 }}
                      padding="normal"
                    >
                      LIQUIDACIÓN DE COTIZACIONES C.P.S.
                    </Typography>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Concepto
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Tasa %
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Aporte bruto
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Deducciones
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Total
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Enfermedad - Maternidad
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("diseaseTasa")}
                      type="number"
                      autoComplete="diseaseTasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Aporte bruto"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("diseaseBruto")}
                      type="number"
                      autoComplete="diseaseBruto"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("diseaseDeduction")}
                      type="number"
                      autoComplete="diseaseDeduction"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("diseaseTotal")}
                      type="number"
                      autoComplete="diseaseTotal"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Intereses
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("interestTasa")}
                      type="number"
                      autoComplete="interestTasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Aporte bruto"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("interestBruto")}
                      type="number"
                      autoComplete="interestBruto"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("interestDeduction")}
                      type="number"
                      autoComplete="interestDeduction"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("interestTotal")}
                      type="number"
                      autoComplete="interestTotal"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Multas
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("finesTasa")}
                      type="number"
                      autoComplete="finesTasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Aporte bruto"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("finesBruto")}
                      type="number"
                      autoComplete="finesBruto"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("finesDeduction")}
                      type="number"
                      autoComplete="finesDeduction"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("finesTotal")}
                      type="number"
                      autoComplete="finesTotal"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Gastos Judiciales
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("judicialTasa")}
                      type="number"
                      autoComplete="judicialTasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Aporte bruto"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("judicialBruto")}
                      type="number"
                      autoComplete="judicialBruto"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("judicialDeduction")}
                      type="number"
                      autoComplete="judicialDeduction"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("judicialTotal")}
                      type="number"
                      autoComplete="judicialTotal"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Otros
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Tasa"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("otherTasa")}
                      type="number"
                      autoComplete="otherTasa"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Aporte bruto"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("otherBruto")}
                      type="number"
                      autoComplete="otherBruto"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Deducciones"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("otherDeduction")}
                      type="number"
                      autoComplete="otherDeduction"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("otherTotal")}
                      type="number"
                      autoComplete="otherTotal"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    colSpan={3}
                    rowSpan={3}
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Observaciones"
                      variant="outlined"
                      margin="normal"
                      multiline
                      maxRows={4}
                      fullWidth
                      {...register("observation")}
                      type="number"
                      autoComplete="observation"
                    />
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <Typography align="center">Importe Liquidación</Typography>
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Importe Liquidación"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("settlementAmount")}
                      type="number"
                      autoComplete="settlementAmount"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Total Aporte a Paper
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Total a Pagar"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("totalPage")}
                      type="number"
                      autoComplete="totalPage"
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    Diferencias
                  </TableCell>
                  <TableCell
                    sx={{ border: "1px solid rgba(102, 102, 102, 1)" }}
                  >
                    <TextField
                      label="Diferencia"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      {...register("difference")}
                      type="number"
                      autoComplete="difference"
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
      <Grid container sx={{ mt: 2 }} justifyContent="center" spacing={2}>
        <Grid item xs={12} md={5} lg={3}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            onClick={() => handleBack()}
          >
            Anterior
          </Button>
        </Grid>
        <Grid item xs={12} md={5} lg={3}>
          <Button type="submit" fullWidth variant="contained" color="primary">
            Siguiente
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default SettlementQuotes;
