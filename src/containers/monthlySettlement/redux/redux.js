import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  initialData: {},
  AmountReceived: {},
  EmployerDeclaration: {},
  SettlementQuotes: {},
};
export const MonthlySeelementSlice = createSlice({
  name: "MonthlySeelement",
  initialState,
  reducers: {
    AmountReceivedSuccess: (state, action) => {
      state.AmountReceived = action.payload;
    },
    EmployerDeclarationSuccess: (state, action) => {
      state.EmployerDeclaration = action.payload;
    },
    SettlementQuotesSuccess: (state, action) => {
      state.SettlementQuotes = action.payload;
    },
    initialDataSuccess: (state, action) => {
      state.initialData = action.payload;
    },
  },
});

export const {
  EmployerDeclarationSuccess,
  AmountReceivedSuccess,
  SettlementQuotesSuccess,
  initialDataSuccess,
} = MonthlySeelementSlice.actions;
export default MonthlySeelementSlice.reducer;
