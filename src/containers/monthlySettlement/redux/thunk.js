import { post } from "../../../utils/request";
import {
  showGlobalLoader,
  hideGlobalLoader,
  messageSuccess,
  messageError,
} from "../../../redux/globalRedux";
export const sendMonthlyFormStart = (data, navigate) => {
  return async (dispatch, getState) => {
    try {
      dispatch(showGlobalLoader());
      const response = await post("/MonthlyForm/create", {
        ...getState().monthlySeelement.SettlementQuotes,
        ...getState().monthlySeelement.EmployerDeclaration,
        ...getState().monthlySeelement.AmountReceived,
        ...data,
      });
      dispatch(messageSuccess(`Se creo con éxito el registro ${response.id}`));
      navigate("/");
    } catch (error) {
      dispatch(messageError("Ocurrió un error y no se pudo registrar"));
    } finally {
      dispatch(hideGlobalLoader());
    }
  };
};
