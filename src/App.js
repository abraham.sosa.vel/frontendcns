import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import AppRouter from "./routes";
import { Theme } from "./theme/BaseStyles";
import { store } from "./redux/store";
import Alerts from "./pages/Alerts";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import "dayjs/locale/es";

function App() {
  return (
    <Router basename="/">
      <Provider store={store}>
        <Theme>
          <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={"es"}>
            <AppRouter />
            <Alerts />
          </LocalizationProvider>
        </Theme>
      </Provider>
    </Router>
  );
}
export default App;
