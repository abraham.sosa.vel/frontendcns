import React from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router";

const PermitionAccess = ({ permissions, children }) => {
  const { auth } = useSelector((state) => state.auth);
  const userRol = auth?.user?.role?.nombre;
  const validate = permissions.find((e) => e === userRol);
  return !!validate ? children : <Navigate to="/" />;
};

export default PermitionAccess;
