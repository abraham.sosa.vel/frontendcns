import React, { lazy, Suspense } from "react";
import { Navigate, Outlet, Route, Routes } from "react-router";
import { CircularProgress, Grid } from "@mui/material";
import ErrorBoundary from "../components/errorBoundary";
import { exceptionDialog } from "../components/exceptionDialog";
import PermitionAccess from "./PermitionAccess";
import { PERMISSIONS } from "../utils/roles";
import Error404 from "../pages/404";

const LoginScreen = lazy(() => import("../pages/Auth"));
//admin
const GeneralPage = lazy(() => import("../pages/GeneralPage"));
const MonthlySettlementForm = lazy(() =>
  import("../pages/MonthlySettlementForm")
);

const Progress = () => (
  <Grid container alignItems="center" justifyContent="center">
    <CircularProgress size={35} />
  </Grid>
);
const AppRoutes = ({ loggedIn }) => {
  return (
    <Routes>
      <Route
        path="/login"
        element={!loggedIn ? <LoginScreen /> : <Navigate to="/" />}
      />
      <Route
        path="/"
        element={loggedIn ? <Outlet /> : <Navigate to="/login" />}
      >
        <Route
          path="/"
          element={
            <Suspense fallback={<Progress />}>
              <ErrorBoundary fallback={exceptionDialog}>
                <GeneralPage />
              </ErrorBoundary>
            </Suspense>
          }
        />
        <Route
          path="/monthlySettlement"
          element={
            <Suspense fallback={<Progress />}>
              <ErrorBoundary fallback={exceptionDialog}>
                <MonthlySettlementForm />
              </ErrorBoundary>
            </Suspense>
          }
        />
        <Route
          path="*"
          element={
            <Suspense fallback={<Progress />}>
              <ErrorBoundary fallback={exceptionDialog}>
                <Error404 />
              </ErrorBoundary>
            </Suspense>
          }
        />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
