import { LogoutRounded } from "@mui/icons-material";
import {
  Avatar,
  ButtonBase,
  Divider,
  Grid,
  MenuItem,
  Stack,
  Typography,
  useTheme,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import ButtonTheme from "../../../components/ButtonTheme/ButtonTheme";
import MenuPopover from "../../../components/MenuPopover";
import { logOutSuccess } from "../../../containers/auth/redux/redux";
const HeaderContent = () => {
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const onLogout = () => {
    dispatch(logOutSuccess());
    localStorage.removeItem("userData");
  };
  return (
    <>
      <Grid item xs={12} container alignItems="center" justifyContent="end">
        <ButtonBase
          sx={{
            p: 0.25,
            bgcolor: open
              ? theme.palette.mode === "dark"
                ? grey[800]
                : grey[300]
              : "transparent",
            borderRadius: 1,
            "&:hover": { bgcolor: "secondary.lighter" },
          }}
          aria-label="open profile"
          aria-controls={open ? "profile-grow" : undefined}
          aria-expanded={open ? "true" : undefined}
          aria-haspopup="true"
          onClick={handleClick}
        >
          <Stack
            direction="row"
            spacing={2}
            alignItems="center"
            sx={{ p: 0.5 }}
          >
            <Avatar alt="profile user" sx={{ width: 32, height: 32 }} />
            <Typography variant="subtitle1">Administrador</Typography>
          </Stack>
        </ButtonBase>

        <MenuPopover
          open={Boolean(open)}
          anchorEl={anchorEl}
          onClose={handleClose}
          sx={{
            p: 0,
            mt: 1.5,
            ml: 0.75,
            "& .MuiMenuItem-root": {
              typography: "body2",
              borderRadius: 0.75,
            },
          }}
        >
          <Stack sx={{ p: 1 }}>
            <MenuItem>
              <ButtonTheme />
            </MenuItem>
          </Stack>

          <Divider sx={{ borderStyle: "dashed" }} />

          <MenuItem
            onClick={onLogout}
            sx={{ display: "flex", alignItems: "center", gap: 1, m: 1 }}
          >
            <LogoutRounded /> Cerrar sesión
          </MenuItem>
        </MenuPopover>
      </Grid>
    </>
  );
};

export default HeaderContent;
