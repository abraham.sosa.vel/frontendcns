import { Equalizer, InsertDriveFile } from "@mui/icons-material";
import {
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

const ListLink = () => {
  return (
    <List
      sx={{
        width: "100%",
        maxWidth: 360,
        bgcolor: "background.paper",
      }}
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Menu
        </ListSubheader>
      }
    >
      <ListItemButton component={Link} to="/">
        <ListItemIcon>
          <Equalizer />
        </ListItemIcon>
        <ListItemText primary={"Dashboard"} />
      </ListItemButton>
      <ListItemButton component={Link} to="/monthlySettlement">
        <ListItemIcon>
          <InsertDriveFile />
        </ListItemIcon>
        <ListItemText primary={"Liquidación mensual"} />
      </ListItemButton>
    </List>
  );
};

export default ListLink;
