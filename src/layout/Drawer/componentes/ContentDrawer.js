import { Grid } from "@mui/material";
import React from "react";
import style from "./contentDrawer.module.css";
import ListLink from "./ListLink";
const ContentDrawer = () => {
  return (
    <Grid container item xs={12} direction="column">
      <Grid item container xs={1} justifyContent="center" alignContent="center">
        <img className={style.logo} src="/logo.jpeg" alt="logo CNS" />
      </Grid>
      <Grid item container>
        <ListLink />
      </Grid>
    </Grid>
  );
};

export default ContentDrawer;
