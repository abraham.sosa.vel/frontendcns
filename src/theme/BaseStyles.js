import { createTheme, ThemeProvider } from "@mui/material";
import { green, grey } from "@mui/material/colors";
import { useSelector } from "react-redux";

export const Theme = ({ children }) => {
  const { themeSelect } = useSelector((state) => state.global);
  let theme = createTheme({
    palette: {
      mode: themeSelect,
      ...(themeSelect === "light"
        ? {
            primary: {
              main: "rgb(2, 76, 69)",
            },
            background: {
              default: grey[200],
              loginDef: grey[200],
              header: "#fff",
              modal: "#fff",
            },
            text: {
              textSepdavi: green[700],
              TextCuston: grey[900],
              textFields: "#666697",
            },
          }
        : {
            primary: {
              main: "rgb(2, 76, 69)",
            },
            // palette values for dark mode
            divider: grey[600],
            background: {
              default: grey[800],
              paper: grey[900],
              loginDef: grey[900],
              header: grey[900],
              modal: "rgba(59,64,71,1)",
            },
            text: {
              textSepdavi: green[700],
              TextCuston: grey[50],
              textFields: "#c5c6b3",
            },
          }),
    },
    components: {
      MuiInputBase: {
        styleOverrides: {
          root: {
            "& input:-internal-autofill-selected": {
              WebkitBoxShadow:
                themeSelect === "dark"
                  ? "0 0 0 100px rgb(29, 43, 54) inset"
                  : "0 0 0 100px rgb(232, 240, 254) inset",
            },
          },
        },
      },
      MuiFormControlLabel: {
        styleOverrides: {
          root: {
            color:
              themeSelect === "dark"
                ? "#c5c6b3 !important"
                : "#212121 !important",
          },
        },
      },
    },
  });
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
