import {
  AssignmentInd,
  Help,
  LocalPolice,
  Stars,
  SupportAgent,
  Unpublished,
} from "@mui/icons-material";
import { Grid, Paper, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import React from "react";

const SelectRol = ({
  id,
  title,
  description,
  icon,
  children,
  setValue,
  nameControl,
}) => {
  const handleClick = () => {
    setValue(nameControl, id);
  };
  return (
    <Grid xs={12} item sx={{ mt: 1 }}>
      <Paper
        onClick={handleClick}
        sx={[
          {
            p: "1rem 1.5rem",
            borderRadius: 3,
            cursor: "pointer",
            border: "1px solid rgba(0, 0, 0, 0.12)",
            boxShadow: "0",
          },
          (theme) => ({
            bgcolor: theme.palette.mode === "dark" ? "#090D10" : "#ffffff",

            "&:hover": {
              backgroundColor:
                theme.palette.mode === "dark" ? "#10161A" : grey[300],
              transition: "background-color 100ms linear",
            },
          }),
        ]}
      >
        <Grid container>
          <Grid
            item
            xs={1}
            container
            justifyContent="center"
            alignItems="center"
          >
            {icon === "LocalPolice" && <LocalPolice />}
            {icon === "SupportAgent" && <SupportAgent />}
            {icon === "AssignmentInd" && <AssignmentInd />}
            {icon === "Stars" && <Stars />}
            {icon === "Unpublished" && <Unpublished />}
            {icon === "Help" && <Help />}
          </Grid>
          <Grid
            item
            xs={10}
            container
            justifyContent="center"
            direction="column"
            sx={{ px: 2 }}
          >
            <Typography gutterBottom variant="subtitle1" component="div">
              {title}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {description}
            </Typography>
          </Grid>
          <Grid
            item
            xs={1}
            container
            justifyContent="center"
            alignItems="center"
          >
            {children}
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default SelectRol;
