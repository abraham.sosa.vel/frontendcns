import React from 'react';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: undefined, errorInfo: undefined };
    this.resetError = this.resetError.bind(this);
  }

  resetError() {
    if (this.state.error) {
      this.setState({ error: undefined, errorInfo: undefined });
    }
  }
  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
  }

  render() {
    if (this.state.error) {
      return <>{this.props.fallback({ resetError: this.resetError })}</>;
    }

    return this.props.children;
  }
}
