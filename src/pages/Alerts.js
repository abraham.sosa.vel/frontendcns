import { Alert, Snackbar } from "@mui/material";
import React, { useEffect, useState } from "react";
import { connect, useSelector } from "react-redux";

const Alerts = ({ message }) => {
  const [open, setOpen] = useState(true);
  useEffect(() => {
    setOpen(message.status);
  }, [message]);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert
        onClose={handleClose}
        severity={message.type}
        sx={{ width: "100%" }}
      >
        {message.message}
      </Alert>
    </Snackbar>
  );
};
const mapStateProps = ({ global }) => {
  return {
    message: global.message,
  };
};
export default connect(mapStateProps)(Alerts);
