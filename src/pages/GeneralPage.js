import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import General from "../containers/generalPage/General";
import { initialRequestGeneralStart } from "../containers/generalPage/redux/thunk";

const GeneralPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(initialRequestGeneralStart());
  }, []);

  return <General />;
};

export default GeneralPage;
