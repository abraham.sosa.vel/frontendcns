import { configureStore } from "@reduxjs/toolkit";
import GlobalStoreReducer from "./globalRedux";
import authReducer from "../containers/auth/redux/redux";
import MonthlySeelementReducer from "../containers/monthlySettlement/redux/redux";
import generalReducer from "../containers/generalPage/redux/redux";
export const store = configureStore({
  reducer: {
    global: GlobalStoreReducer,
    auth: authReducer,
    monthlySeelement: MonthlySeelementReducer,
    generalReporter: generalReducer,
  },
});
